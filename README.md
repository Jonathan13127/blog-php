# Éxercice

Votre objectif est de réaliser un mini-site de blog. 
Un utilisateur pourra : 

- s'enregistrer
- se connecter
- ajouter un post (en étant connecté)
- visualiser sa liste de post (en étant connecté)
- se déconnecter

## Consignes : 

- vous enverrez par mail un lien github/gitlab public, contenant l'ensemble de vos fichiers
- Le présent fichier sera à la racine de votre git, nommé README.md. Vous y aurez cocher les cases des fonctionnalités réalisées, en indiquant à côté de la case à cocher le prénom de la personne qui à réalisé l'étape. 

## Groupe : 

MALLIA Jonathan, ASLAN Matthieu,MAGGIORINO VEGLIO Riccardo

## Étapes : 

### Enregistrement : 

 - [x] **Jonathan** Front
   - [x] **Jonathan** formulaire d'enregistrement :
     - [x] **Jonathan** email
    <!--  - [ ] **prénom** pseudo -->
     - [x] **Jonathan** mot de passe
     - [x] **Jonathan** confirmation du mot de passe
 - [x] **Jonathan** back :
   - [x] **Jonathan** validation et nettoyage des données saisies
   - [x] **Jonathan** Connexion à la base de données
     - [x] **Jonathan** vérification que l'email <!-- ou le pseudo --> ne sont pas déjà utilisés
   - [x] **Jonathan** vérification que les mots de passes saisis sont identiques (mot de passe et mot de passe de confirmation)
   - [x] **Jonathan** Si tout OK, 
     - [x] **Jonathan** hash du mot de passe
     - [x] **Jonathan** enregistrement de l'utilisateur dans la base, avec mot de passe hashé
     - [x] **Jonathan** création de session
     - [x] **Jonathan** redirection sur la page de saisie de post
   - [x] **Jonathan** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à l'enregistrement

### Login 

 - [x] **prénom** Front
   - [x] **prénom** formulaire de connexion :
     - [x] **prénom** un champ email
     - [x] **prénom** un champ mot de passe
 - [x] **prénom** Back 
   - [x] **prénom** Validation et récupération des données
   - [x] **prénom** Recherche de l'utilisateur dans la base de donnée
     - [x] **prénom** Si trouvé, vérification du mot de passe
       - [x] **prénom** Si ok, création de la session, et redirection sur la page des posts
   - [x] **prénom** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à la connexion

## Affichage des posts : 

 - [x] **prénom** Vérification de l'état de connexion de l'utilisateur 
   - [x] **prénom** Si connecté, 
     - [x] **prénom** recherche de tous les posts de l'utilisateur en base de données
     - [x] **prénom** parcours et affichage des posts
     - [x] **prénom** affichage de liens
       - [x] **prénom** vers la saisie d'un post
       - [x] **prénom** pour se déconnecter
   - [x] **prénom** si non connecté : redirection vers la page de connexion

## Saisie d'un post

 - [x] **prénom** Front (PHP + html): 
   - [x] **prénom** si utilisateur connecté
     - [x] **prénom** formulaire de saisie du post + bouton d'envoi
     - [x] **prénom** lien de retour à l'affichage des posts de l'utilisateur
   - [x] **prénom** Si non connecté : redirection vers la page de connexion
 - [x] **prénom** Back : 
   - [x] **prénom** Validation et récupération des données
   - [x] **prénom** Connexion à la BDD
   - [x] **prénom** Préparation de la requête d'insertion du post
   - [x] **prénom** association des paramètres
   - [x] **prénom** execution de la requête
   - [x] **prénom** Si tout s'est bien passé, retour à la liste des posts
