<?php 

//On récolte les données du formulaire et on les stocke dans des variables 
//On nettoie les variable pour éviter les failles xss

filter_input_array(INPUT_POST, 
[
    "mail" => FILTER_SANITIZE_STRING, 
    "pwd" => FILTER_SANITIZE_STRING,
    "pwd2" => FILTER_SANITIZE_STRING,
]
);

$mail = $_POST["mail"];
$pwd = $_POST["pwd"];
$pwd2 = $_POST["pwd2"];

try{
    include('db.php');
    $conn = new PDO(DB_URL, DB_USER, DB_PASS);

    try{
        $rqt = <<<SQL
                SELECT email FROM users WHERE email = :email
            SQL;
            // Préparer la requête
            $stmtCount = $conn->prepare($rqt);
            // Associer les paramètres
            $stmtCount->bindParam(":email", $mail, PDO::PARAM_STR);     
            // Exécuter la requête
            $nb = $stmtCount->execute(); 
            $user = $stmtCount->fetch();

            if($user){ //Si le compte existe  

                echo "Utilisateur existant";
                
            }else{// Si l'utilisateur existe pas 
                if($pwd == $pwd2){//Si les mot de passe sont identiques

                    $pwd = password_hash($pwd, PASSWORD_DEFAULT); 
            
                    try{
                        $rqt = <<<SQL
                                INSERT INTO users (email, password)
                                VALUES (:email , :password);
                            SQL;
                                // Préparer la requête
                            $stmtInsert = $conn->prepare($rqt);
                                // Associer les paramètres
                            $stmtInsert->bindParam(":email", $mail, PDO::PARAM_STR); 
                            $stmtInsert->bindParam(":password", $pwd, PDO::PARAM_STR); 
                    
                            // Exécuter la requête
                            $nb = $stmtInsert->execute();

                            if(session_status() != PHP_SESSION_ACTIVE) {
                                session_start();
                                $_SESSION["userid"] = $user[0];
                                $_SESSION["email"] = $mail;
                                header("Location:../post/post.php");
                                exit;
                            }
                    }catch(Exception $e){
                        $e->getMessage();
                        echo $e;
                    }
                }else{
                    echo "Mot  de passe ne sont pas les mêmes";
                }
           }
    }catch(Exception $e){
        $e->getMessage();
        echo $e;
    }
}catch(Exception $e){
    echo $e;
}

?>