<?php

/**- Validation et récupération des données**/

filter_input_array(INPUT_POST, 
[
    "mail" => FILTER_SANITIZE_STRING, 
    "pwd" => FILTER_SANITIZE_STRING,
]
);

$mail = $_POST["mail"];
$pwd = $_POST["pwd"];

try {
    include('db.php');
    $conn = new PDO(DB_URL, DB_USER, DB_PASS);
    /**- Recherche de l'utilisateur dans la base de donnée**/

    try {
        
        $rqt = <<<SQL
        SELECT * FROM users WHERE email = :email;
        SQL;
        // Préparer la requête
        $stmtCount = $conn->prepare($rqt);
        // Associer les paramètres
        $stmtCount->bindParam(":email", $mail, PDO::PARAM_STR);     
        // Exécuter la requête
        $nb = $stmtCount->execute(); 
        $user = $stmtCount->fetch();

        /**- Si trouvé, vérification du mot de passe**/
        if($user[1] == $mail && password_verify($pwd,$user[2])){

            /**- Si ok, création de la session, et redirection sur la page des posts**/
            if(session_status() != PHP_SESSION_ACTIVE) {
                session_start();
                $_SESSION["userid"] = $user[0];
                $_SESSION["email"] = $mail;
                header("Location:../post/post.php");
                exit;
            }

        /**- Si erreur, affichage de message d'erreur, et d'un lien pour revenir à la connexion**/
        } else {
            echo $mail." n'a pas étais trouvé ou ne correspond pas avec le mdp : ".$pwd;
            echo "<form action='index.php'><input type='submit' value='Go to Login'></form>";
        }

    } catch (Exception $e){
        $e->getMessage();
        echo $e;
    }
} catch (Exception $e){
    $e->getMessage();
    echo $e;
}

?>