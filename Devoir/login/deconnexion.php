<?php

session_start();
session_unset();
session_destroy();
session_write_close();
session_regenerate_id(true);
setcookie(session_name(),'',strtotime('-1 day'),'');

header('Location:../index.php');

exit();
?>