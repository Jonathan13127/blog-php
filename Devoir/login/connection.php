<?php

/* Démarrage du mécanisme de session si celui-ci n'est pas démarré */
if(session_status() != PHP_SESSION_ACTIVE) {
    session_start();
}

/**
 * Indique si un utilisateur est connecté
 * 
 * Un utilisateur est connecté s'il a une session en cours. 
 * Pour vérifier s'il a une session en cours, on peut tester la présence d'une variable de session 
 * (par exemple son pseudo, son email ou son id)
 *
 * @return boolean true si l'utilisateur est bine connecté
 */
function connected() : bool {
    // TODO : implémenter le corps de la fonction et retourner la bonne valeur
    return false;
}

/**
 * Fonction qui permet la déconnexion d'un utilisateur
 *
 * @return void
 */
function disconnect() : void {

}