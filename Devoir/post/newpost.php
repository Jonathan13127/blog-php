<?php
/**
 * post TEXT NOT NULL,
 * user_id INT NOT NULL,
 */

/**- Validation et récupération des données**/

include('../login/connection.php');
include('../login/areuconnected.php');

filter_input_array(INPUT_POST, 
[
    "message" => FILTER_SANITIZE_STRING, 
]
);

$message = $_POST["message"];

try {
    include('../login/db.php');
    $conn = new PDO(DB_URL, DB_USER, DB_PASS);
    try {
        $rqt = <<<SQL
        INSERT INTO posts (post,user_id) values (:post,:userid)
        SQL;
        // Préparer la requête
        $postInsert = $conn->prepare($rqt);
        // Associer les paramètres
        $postInsert->bindParam(":post", $message, PDO::PARAM_STR);
        $postInsert->bindParam(":userid", $_SESSION["userid"], PDO::PARAM_INT);     
        // Exécuter la requête
        $nb = $postInsert->execute();

        echo $_SESSION["userid"];
        echo $_SESSION["email"];

        header('Location:post.php');
        exit;

    } catch (Exception $e){
        $e->getMessage();
        echo $e;
    }
} catch (Exception $e){
    $e->getMessage();
    echo $e;
}

echo "echec";

?>

