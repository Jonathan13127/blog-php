<?php

include('../login/connection.php');
include('../login/areuconnected.php');
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>My post - BISCORB © ECO++</title>
</head>
<body>
    <header>
        <center><h1><?php echo $_SESSION["email"];?></h1></center>
        <ul>
            <li>
            <form action="newpost.php" method="post">
            <input type="text" name="message" id="message">
            <button type="submit"> NEW POST</button>
            </form>
            </li>
            <li>
                <form action="post.php">
                <input type='submit' value='FILL'>
                </form>
            </li>
            <li>
                <form action="../login/deconnexion.php">
                <input type='submit' value='Sign Out'>
                </form>
            </li>
        </ul>
    </header>
    <main>
        <div class="post">
            <?php include('readmypost.php');?>
        </div>
    </main>    
</body>
</html>