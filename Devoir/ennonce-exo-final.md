# Éxercice

Votre objectif est de réaliser un mini-site de blog. 
Un utilisateur pourra : 

- s'enregistrer
- se connecter
- ajouter un post (en étant connecté)
- visualiser sa liste de post (en étant connecté)
- se déconnecter

## Consignes : 

- vous enverrez par mail un lien github/gitlab public, contenant l'ensemble de vos fichiers
- Le présent fichier sera à la racine de votre git, nommé README.md. Vous y aurez cocher les cases des fonctionnalités réalisées, en indiquant à côté de la case à cocher le prénom de la personne qui à réalisé l'étape. 

## Groupe : 

:warning: TODO : indiquer la composition du groupe ici 

## Étapes : 

### Enregistrement : 

 - [ ] **prénom** Front
   - [ ] **prénom** formulaire d'enregistrement :
     - [ ] **prénom** email
    <!--  - [ ] **prénom** pseudo -->
     - [ ] **prénom** mot de passe
     - [ ] **prénom** confirmation du mot de passe
 - [ ] **prénom** back :
   - [ ] **prénom** validation et nettoyage des données saisies
   - [ ] **prénom** Connexion à la base de données
     - [ ] **prénom** vérification que l'email <!-- ou le pseudo --> ne sont pas déjà utilisés
   - [ ] **prénom** vérification que les mots de passes saisis sont identiques (mot de passe et mot de passe de confirmation)
   - [ ] **prénom** Si tout OK, 
     - [ ] **prénom** hash du mot de passe
     - [ ] **prénom** enregistrement de l'utilisateur dans la base, avec mot de passe hashé
     - [ ] **prénom** création de session
     - [ ] **prénom** redirection sur la page de saisie de post
   - [ ] **prénom** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à l'enregistrement

### Login 

 - [ ] **prénom** Front
   - [ ] **prénom** formulaire de connexion :
     - [ ] **prénom** un champ email
     - [ ] **prénom** un champ mot de passe
 - [ ] **prénom** Back 
   - [ ] **prénom** Validation et récupération des données
   - [ ] **prénom** Recherche de l'utilisateur dans la base de donnée
     - [ ] **prénom** Si trouvé, vérification du mot de passe
       - [ ] **prénom** Si ok, création de la session, et redirection sur la page des posts
   - [ ] **prénom** Si erreur, affichage de message d'erreur, et d'un lien pour revenir à la connexion

## Affichage des posts : 

 - [ ] **prénom** Vérification de l'état de connexion de l'utilisateur 
   - [ ] **prénom** Si connecté, 
     - [ ] **prénom** recherche de tous les posts de l'utilisateur en base de données
     - [ ] **prénom** parcours et affichage des posts
     - [ ] **prénom** affichage de liens
       - [ ] **prénom** vers la saisie d'un post
       - [ ] **prénom** pour se déconnecter
   - [ ] **prénom** si non connecté : redirection vers la page de connexion

## Saisie d'un post

 - [ ] **prénom** Front (PHP + html): 
   - [ ] **prénom** si utilisateur connecté
     - [ ] **prénom** formulaire de saisie du post + bouton d'envoi
     - [ ] **prénom** lien de retour à l'affichage des posts de l'utilisateur
   - [ ] **prénom** Si non connecté : redirection vers la page de connexion
 - [ ] **prénom** Back : 
   - [ ] **prénom** Validation et récupération des données
   - [ ] **prénom** Connexion à la BDD
   - [ ] **prénom** Préparation de la requête d'insertion du post
   - [ ] **prénom** association des paramètres
   - [ ] **prénom** execution de la requête
   - [ ] **prénom** Si tout s'est bien passé, retour à la liste des posts